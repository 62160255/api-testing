const mongoose = require('mongoose')
const { Schema } = mongoose
const builgingSchema = Schema({
  name: { type: String, unique: true },
  level: Number
})
module.exports = mongoose.model('Building', builgingSchema)
